

static Handle kzmodjsCookie;
static int settings[MAXPLAYERS + 1];

void OnPluginStart_Clientprefs()
{
	kzmodjsCookie = RegClientCookie("kzmod_js_cookie_v2", "Cookie for KZMod Jumpstats", CookieAccess_Private);
	if (kzmodjsCookie == INVALID_HANDLE)
	{
		SetFailState("Couldn't create KZMod Jumpstats cookie.");
	}
}

void OnClientCookiesCached_Clientprefs(int client)
{
	char buffer[MAX_COOKIE_SIZE];
	GetClientCookie(client, kzmodjsCookie, buffer, sizeof(buffer));
	
	settings[client] = StringToInt(buffer);
}

void SaveClientCookies(int client)
{
	if (!GCIsValidClient(client) || !AreClientCookiesCached(client))
	{
		return;
	}
	
	char buffer[MAX_COOKIE_SIZE];
	IntToString(settings[client], buffer, sizeof(buffer));
	SetClientCookie(client, kzmodjsCookie, buffer);
}

bool IsSettingEnabled(int client, int setting)
{
	if (GCIsValidClient(client))
	{
		return !!(settings[client] & setting);
	}
	return false;
}

void ToggleSetting(int client, int setting)
{
	if (GCIsValidClient(client))
	{
		settings[client] ^= setting;
		SaveClientCookies(client);
	}
}